"""
Module defined to wrap default python random module.

It is intended for testing purpose.
By setting it to "inactive", it disables the randomness.
"""
from random import choice as rand_choice, randint as rand_randint, uniform as rand_uniform

ACTIVE = True


def activate(val):
    """ Activate or deactivate the randomness. """
    global ACTIVE
    ACTIVE = val


def choice(choice_items):
    """ Stub around 'random.choice'. """
    global ACTIVE
    if ACTIVE:
        return rand_choice(choice_items)
    return choice_items[0]


def randint(a, b):
    """ Stub around 'random.randint'. """
    global ACTIVE
    if ACTIVE:
        return rand_randint(a, b)
    return (a + b) / 2


def uniform(a, b):
    """ Stub around 'random.uniform'. """
    global ACTIVE
    if ACTIVE:
        return rand_uniform(a, b)
    return 0.5 * (a + b)


