"""
Module that defines the possible player actions and associated effects
"""

from .geopolitics import REPUBLICANS


class Action:
    """
    A possible player action.

    They can be one of two categories:
    - a fake news to spread
    - a statement to make
    - an effect associated to the action
    """
    def __init__(self, name, description, effect):
        self.name = name
        self.desc = description
        self.effect = effect

    def __str__(self):
        return self.name + "\n\n" + self.desc


class Effect:
    def __init__(self, category, targets):
        self.category = category
        self.target_list = targets
        self._targets = []

    def apply(self):
        """ Apply the effect to the target states """
        pass

    def link_targets(self, country):
        """ Link the effect to the actual state objects """
        self._targets = []
        if self.target_list == "*":
            for target in country:
                self._targets.append(target)
        else:
            for target in self.target_list.split(","):
                target_abbrev = target.strip()
                self._targets.append(country.states[target_abbrev])


class PowerStatement(Effect):
    """
    Direct vote change effect.
    Increases votes by convincing an amount of people to vote for your party.
    """
    def __init__(self, targets, amount):
        super(PowerStatement, self).__init__("Power Statement", targets)
        self.amount = amount

    def apply(self):
        """ Apply the effect to the target states """
        for state in self._targets:
            state.convince_voters(REPUBLICANS, self.amount)


class FakeNews(Effect):
    """
    Indirect vote change effect.
    Increases votes by:
     - providing a multiplier the number of people voting for you (as a result of a SwingEffect)
     - reducing the number of voters for your opponent if you STOP THE COUNT
    If the effect duration (in number of turns) expires, the boost is lost.
    """
    def __init__(self, targets, bonus, duration):
        super(FakeNews, self).__init__("Fake News", targets)
        self.bonus = bonus
        self.duration = duration

    def apply(self):
        """ Apply the effect to the target states """
        # Apply the effect to the target states
        for state in self._targets:
            state.modify_bias(self.bonus)

        # Decrement the boost life span
        self.duration -= 1

    def alive(self):
        return self.duration > 1
