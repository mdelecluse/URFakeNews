
from pygame.font import SysFont

SMALL_FONT = SysFont('Fixed Sys', 16)
NORMAL_FONT = SysFont('Fixed Sys', 24)
HUGE_FONT = SysFont('Fixed Sys', 48)