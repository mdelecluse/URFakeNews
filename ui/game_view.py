"""
Definition of the game general UI layout

 Geopolitics view is defined in another file
"""

from pygame import display, Rect, image
from pygame.draw import rect as draw_rect

from gameplay.geopolitics import DEMOCRATS, REPUBLICANS, UNDECIDED

from .colors import *
from .text import write_text
from .fonts import *


# positions and size of the ui
DISPLAY_WIDTH = 1280
DISPLAY_HEIGHT = 800

# thickness of the lines / borders / rectangles being drawn
THICKNESS = 3
# spacing between elements
SPACING = 2 * THICKNESS

# Geopolitical map offset
MAP_OFFSET = (280, 150)

twitter_y = 40
logo_size = 50
fake_news_x1 = 10
fake_news_x2 = 270
fake_news_header_y = 150
fake_news_y = 200
fake_news_height = 100
button_bottom_y = 750
election_y = 100
bar_height = 40

bar_x1 = MAP_OFFSET[0] + 2 * SPACING + logo_size
bar_x2 = 900
bar_info_x = bar_x2 + logo_size + 2 * SPACING

STATE_INFO_X = bar_info_x + SPACING
STATE_INFO_Y = 0
STATE_INFO_WIDTH = DISPLAY_WIDTH - STATE_INFO_X
STATE_INFO_HEIGHT = MAP_OFFSET[1] - 2 * THICKNESS
STATE_INFO_TXT_X = STATE_INFO_X + 10
STATE_INFO_TXT_Y = STATE_INFO_Y + 2 * SPACING

# other constants
nb_fake_news = 4


class GameView:

    def __init__(self, game):
        """ The game UID representation """
        from .geopolitics_view import CountryView

        self.screen = display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
        self._rect = Rect(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT)

        self._game = game  # type: Country
        self._country_view = CountryView(game.country, self.screen, MAP_OFFSET)
        self._state_selected = None

        self.fake_news_button_list = []
        self._state_selected = None
        self._fake_news_selected = None
        self.current_turn = -1

    # def draw_fake_news(self):
    #     for i, fake_news in enumerate(self.fake_news_list):
    #         self.fake_news_button_list.append()

    def draw_fake_news(self):
        if self.current_turn != self._game.nb_turns:
            print("changed")
            self.fake_news_button_list = []
            self.current_turn = self._game.nb_turns

            for i, action in enumerate(self._game.possible_actions):
                action_ui = FakeNewsUi(self.screen, i, action)
                self.fake_news_button_list.append(action_ui)

        for button in self.fake_news_button_list:
            button.draw()

    def draw(self):
        """ Draw the game screen and the child elements """
        draw_rect(self.screen, WHITE, self._rect)
        self._country_view.draw()
        is_clickable = (self._state_selected is not None) and (self._fake_news_selected is not None)
        self.button_next = self.load_button_next(is_clickable)
        self.button_stop_the_count = self.load_stop_the_count()

        self.draw_fake_news()
        self._draw_state_info()
        self.load_game_ui()

    def click(self, click_position):

        # Handle click on states
        state_clicked = self._country_view.click(click_position)
        if state_clicked is not None:
            self._state_selected = state_clicked

        if self.button_next.click(click_position):
            if self._fake_news_selected is not None and self._state_selected is not None:
                move_index = self._fake_news_selected.index

                action, state = self._game.play(move_index, self._state_selected)
                print("Playing action: %s" % action.name)
                print("Rallying to: %s" % state.name)

                self._game.take_turn()

                self._fake_news_selected = None
                self._state_selected = None

        if self.button_stop_the_count.click(click_position):
            self._game.stop_the_count()

        for button in self.fake_news_button_list:
            if button.click(click_position):
                self._fake_news_selected = button
        if self._fake_news_selected is not None:
            for button in self.fake_news_button_list:
                if button == self._fake_news_selected:
                    button.set_highlight(True)
                else:
                    button.set_highlight(False)

        # print(self._game.country.count_electors())

    def load_game_ui(self):
        self.load_twitter_bar(10)
        self.load_twitter_logo()
        self.load_twitter_cage()
        self.load_vote_bar()
        self.load_fake_news_header()
        self.load_republican_logo()
        self.load_democrat_logo()
        self.load_line_info()

    def load_twitter_logo(self):
        twitter_logo = image.load('res/img/twitter_logo.png')
        height = twitter_y + (bar_height - logo_size) / 2
        self.screen.blit(twitter_logo, (MAP_OFFSET[0] + SPACING, height))

    def load_twitter_cage(self):
        twitter_cage = image.load('res/img/twitter_cage.png')
        self.screen.blit(twitter_cage, (bar_x2 + SPACING, SPACING))

    def load_republican_logo(self):
        republican_logo = image.load('res/img/republican.png')
        self.screen.blit(republican_logo, (bar_x2 + SPACING, election_y - SPACING))

    def load_democrat_logo(self):
        democrat_logo = image.load('res/img/democrat.png')
        self.screen.blit(democrat_logo, (MAP_OFFSET[0] + SPACING, election_y - SPACING))

    def load_twitter_bar(self, pct=0):
        """
        load the twitter bar at the top of the game
        :param pct: percentage in 0, 100
        """
        if pct > 100:
            pct = 100
        elif pct < 0:
            pct = 0
        progression = int((bar_x2-bar_x1) * pct / 100)
        draw_rect(self.screen, TWITTER_INNER_COLOR, (bar_x1, twitter_y, progression, bar_height))
        draw_rect(self.screen, TWITTER_OUTER_COLOR, (bar_x1, twitter_y, bar_x2-bar_x1, bar_height), THICKNESS)

    def load_vote_bar(self,):
        """
        color the bar at the top of the game
        :param dem: number of democrats votes
        :param undecided: number of people undecided
        :param rep: number of republicans votes
        """
        votes = self._game.country.count_electors()
        dem = votes[DEMOCRATS]
        undecided = votes[UNDECIDED]
        rep = votes[REPUBLICANS]

        tot_votes = dem + undecided + rep

        progression_dem = int(dem * (bar_x2-bar_x1) / tot_votes)
        starting_undecided = progression_dem + bar_x1
        progression_undecided = int(undecided * (bar_x2-bar_x1) / tot_votes)
        starting_rep = starting_undecided + progression_undecided
        progression_rep = bar_x2 - bar_x1 - progression_undecided - progression_dem

        # votes
        draw_rect(self.screen, BLUE, (bar_x1, election_y, progression_dem, bar_height))  # dem
        draw_rect(self.screen, LIGHT_GRAY, (starting_undecided, election_y, progression_undecided, bar_height))
        draw_rect(self.screen, RED, (starting_rep, election_y, progression_rep, bar_height))  # rep

        # outer rectangle
        draw_rect(self.screen, DARK_GRAY, (bar_x1, election_y, bar_x2 - bar_x1, bar_height), THICKNESS)

        # line at the middle
        mid = bar_x1 + (bar_x2 - bar_x1 - THICKNESS) / 2
        height_line = 2 * THICKNESS

        draw_rect(self.screen, DARK_GRAY, (mid, election_y, THICKNESS, height_line))
        draw_rect(self.screen, DARK_GRAY, (mid, election_y + bar_height - height_line, THICKNESS, height_line))

    def load_fake_news_header(self):
        write_text(self.screen, "FAKE NEWS", (35, fake_news_header_y + SPACING), HUGE_FONT,
                   MAP_OFFSET[0] - 2 * SPACING, color=BLACK)

    def load_button_next(self, is_clickable=False):
        if is_clickable:
            inner_color = LIGHT_GREEN
            outer_color = GREEN
        else:
            inner_color = LIGHT_GRAY
            outer_color = DARK_GRAY
        width = (fake_news_x2 - fake_news_x1) - 2 * THICKNESS
        height = DISPLAY_HEIGHT - button_bottom_y - SPACING
        button = RectangularButton(self.screen, inner_color, (fake_news_x1, button_bottom_y, width, height))
        # pygame.draw.rect(screen, light_green, pygame.Rect)
        draw_rect(self.screen, outer_color, (fake_news_x1, button_bottom_y, width, height), THICKNESS)
        write_text(self.screen, "NEXT TURN", (fake_news_x1 + 5 * SPACING, button_bottom_y + 5), HUGE_FONT, width, BLACK)
        return button

    def load_stop_the_count(self):
        # pygame.draw.rect(screen, light_gray, pygame.Rect(0, 0, pos_map[0], pos_map[1]))
        button = RectangularButton(self.screen, WHITE, (5, 5, 270, 140))
        stop_the_count = image.load('res/img/stop_the_count_button.png')
        self.screen.blit(stop_the_count, (5, 5))
        return button

    def load_line_info(self):
        draw_rect(self.screen, BLACK, (bar_info_x, 0, THICKNESS, MAP_OFFSET[1] - THICKNESS))

    def _draw_state_info(self):
        """
        write info to the top right corner of the window
        :param text:
        :return:
        """
        draw_rect(
            self.screen,
            BACKGROUND,
            (STATE_INFO_X, STATE_INFO_Y, STATE_INFO_WIDTH, STATE_INFO_HEIGHT)
        )
        if self._state_selected is None:
            return

        state = self._state_selected
        info = f"State: {state.name}\n" \
               f"Electors: {state.electors}\n" \
               f"\n" \
               f"Democrat votes: {state.votes.democrats:.2f}%\n" \
               f"Republicans votes: {state.votes.republicans:.2f}%\n" \
               f"Undecided voters: {state.votes.undecided:.2f}%\n" \
               f"\n" \
               f"Republican bias voters: {state.get_bias():.2f}%\n"
        write_text(self.screen, info, (STATE_INFO_TXT_X, STATE_INFO_TXT_Y), NORMAL_FONT, DISPLAY_WIDTH)


class RectangularButton:
    def __init__(self, screen, color, dimension, width=0):
        self.rectangle = draw_rect(screen, color, dimension, width)

    def click(self, click_position, highlight=False):
        clicked = True if self.rectangle.collidepoint(click_position) else False
        return clicked


class FakeNewsUi:
    def __init__(self, screen, idx, fake_news):
        """
        :param idx: index of the fake news, in [0, nb_fake_news]
        :param fake_news: action
        """
        self.index = idx
        self.height = fake_news_y + idx * (fake_news_height + 2 * THICKNESS)
        self.screen = screen
        self.rectangle = draw_rect(screen, LIGHT_RED, (fake_news_x1, self.height, fake_news_x2 - fake_news_x1, fake_news_height))
        draw_rect(screen, RED, (fake_news_x1, self.height, fake_news_x2 - fake_news_x1, fake_news_height), THICKNESS)
        self.text = str(fake_news)
        write_text(screen, self.text, (fake_news_x1 + 10, self.height + 5), NORMAL_FONT, fake_news_x2 - fake_news_x1 - 10, BLACK)
        self.fake_news = fake_news
        self.highlighted = False

    def set_highlight(self, val):
        self.highlighted = val
        self.draw()

    def get_action(self):
        return self.fake_news

    def click(self, click_position):
        return True if self.rectangle.collidepoint(click_position) else False

    def draw(self):
        self.rectangle = draw_rect(self.screen, LIGHT_RED,
                                   (fake_news_x1, self.height, fake_news_x2 - fake_news_x1, fake_news_height))
        if not self.highlighted:
            draw_rect(self.screen, RED,
                      (fake_news_x1, self.height, fake_news_x2 - fake_news_x1, fake_news_height), THICKNESS)
        else:
            draw_rect(self.screen, GREEN,
                      (fake_news_x1, self.height, fake_news_x2 - fake_news_x1, fake_news_height), 2 * THICKNESS)
        write_text(self.screen, self.text, (fake_news_x1 + 10, self.height + 5), NORMAL_FONT,
                   fake_news_x2 - fake_news_x1 - 10, BLACK)
