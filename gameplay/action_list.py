
from .actions import Action, FakeNews, PowerStatement
from .resources import regions

all_actions = [

    # Regional fake news -> indirect increase in votes per region

    Action(
        "The Great Wall",
        "We are gonna build a wall. A beautiful wall. And Mexico is going to pay for it.",
        FakeNews(regions['Mexico border'], 2.0, 5)  # 100% increase of vote swings for 5 turns
    ),
    Action(
        "The White Walkers",
        "We are gonna build another wall. An ice wall. And Canada is going to pay for it.",
        FakeNews(regions['Canada border'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),

    Action(
        "Trump Tower",
        "I think New York needs a new tower. We lost two to the terrorists, but I am gonna give one back.",
        FakeNews(regions['Northeast'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),
    Action(
        "Professional golfer",
        "You know I love Florida, right ? I go there all the time. Great for golf. We need more golfs.",
        FakeNews(regions['Southeast'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),
    Action(
        "Slim Shady",
        "We used to have great factories. Now everything is in China. I am gonna bring it back here.",
        FakeNews(regions['Midwest'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),
    Action(
        "Tiger King",
        "We need the defend ourselves. We should be able to carry a gun. A have a pet tiger.",
        FakeNews(regions['Southwest'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),
    Action(
        "Hotel California",
        "Las Vegas is the best. Such a lovely place. We need more Vegas. People will come from everywhere.",
        FakeNews(regions['Western'], 1.5, 5)  # 50% increase of vote swings for 5 turns
    ),


    # Global fake news -> indirect increase in votes, nation-wide

    Action(
        "Godzilla",
        "Ever wondered why Joe is so pale ? He is wearing a fake skin. He works for the reptilians. We have proof.",
        FakeNews("*", 1.1, 10)  # 10% increase of vote swings for 10 turns
    ),
    Action(
        "The Outsider",
        "Our campaign team asked Joe to show us his birth certificate. He refused. No wonder he is against the wall...",
        FakeNews("*", 1.25, 2)  # 25% increase of vote swings for 2 turns
    ),
    Action(
        "Just a flu",
        "Covid is not as bad as the 'fake media' says. It's just a flu.",
        FakeNews("*", 1.5, 1)  # 50% increase of vote swings for 1 turns
    ),
    Action(
        "The China Virus",
        "It comes from China. It was developed in a lab in China. In China. China China China.",
        FakeNews("*", 1.5, 1)  # 50% increase of vote swings for 2 turns
    ),

    # Power statements -> direct increase in votes

    Action(
        "Power statement.",
        "You need to grab them by the pussy.",
        PowerStatement("*", 2.5)
    ),
    Action(
        "Power statement.",
        "I understand the economy like no one else.",
        PowerStatement(regions['Northeast'], 5.0)
    ),
    Action(
        "Power statement.",
        "I am the best at golf.",
        PowerStatement(regions['Southeast'], 5.0)
    ),
    Action(
        "Power statement.",
        "I love rap music. Those drums. Bam bum bam.",
        PowerStatement(regions['Midwest'], 5.0)
    ),
    Action(
        "Power statement.",
        "I own guns. Lots of guns. And I can fire them too.",
        PowerStatement(regions['Southwest'], 5.0)
    ),
    Action(
        "Power statement.",
        "I went to California, and peed on the fire. Poof. It's gone.",
        PowerStatement(regions['Western'], 5.0)
    ),
    Action(
        "Power statement.",
        "They are all drug dealers, criminals and rapists.",
        PowerStatement(regions['Mexico border'], 5.0)
    ),
    Action(
        "Power statement.",
        "They are all beaver dealers, mounties and hockey players.",
        PowerStatement(regions['Canada border'], 5.0)
    ),

    # Action(
    #     "Power statement.",
    #     "XXX",
    #     SwingEffect("XXX", 5.0)
    # ),
]
