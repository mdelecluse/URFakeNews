"""
This module defines the voting system:
    - How votes are counted (democrats, republicans, undecided)
    - How states are represented
    - How the country is defined
"""

from helpers.rand import uniform
from .resources import state_abbrev

DEMOCRAT = 'democrats'
DEMOCRATS = DEMOCRAT
REPUBLICAN = 'republicans'
REPUBLICANS = REPUBLICAN
UNDECIDED = 'undecided'
SWING = UNDECIDED

RALLY_VALUE = 20.0  # The increase in vote percentage that a rally gives to the organizer
# TODO: add different value per party
UNDECIDED_RANGE = (10.0, 20.0)  # Between 10 and 20% of total voters will be undecided
DEMOCRATS_RANGE = (35.0, 65.0)  # Between 40 and 60% of decided voters will be democrats


class Votes:
    """
    A vote count for a particular state
    """
    democrats = 40.0
    republicans = 40.0
    undecided = 20.0

    def __init__(self, democrats=40.0, republicans=40.0, undecided=20.0):
        """ Constructor """
        self.democrats = democrats
        self.republicans = republicans
        self.undecided = undecided

        self._uniformize()  # Keep vote counts at 100%

    def _uniformize(self):
        """ Keep the total number of votes at 100% """
        total = self.democrats + self.republicans + self.undecided
        self.democrats = 100.0 * self.democrats / total
        self.republicans = 100.0 * self.republicans / total
        self.undecided = 100.0 * self.undecided / total

    def transfer(self, voters_from, voters_to, number):
        """ Transfer voters from one pool to another """
        # Get the values of categories to add / remove voters from
        category_from = getattr(self, voters_from)
        category_to = getattr(self, voters_to)

        # Ensure that we don't convert more voter than actually available
        capped_number = min(number, category_from)

        # Get the values of categories to add / remove voters from
        setattr(self, voters_from, category_from - capped_number)
        setattr(self, voters_to, category_to + capped_number)

    def __str__(self):
        return f"Democrats: {self.democrats:.2f}%, " \
               f"Republicans: {self.republicans:.2f}%, " \
               f"Undecided: {self.undecided:.2f}%"


class RandomVotes(Votes):
    """
    A vote count for a particular state - initialized at random within some range
    """
    def __init__(self, party_range=(50.0, 50.0)):
        undecided = uniform(*UNDECIDED_RANGE)
        democrats = uniform(*DEMOCRATS_RANGE) * (1. - undecided / 100.0)
        republicans = 100. - democrats - undecided
        super(RandomVotes, self).__init__(democrats, republicans, undecided)


class State:
    """
    A state that you must win over !

    Each state is defined by:
     - its name
     - its number of electors
     - its bias toward republicans
    """

    # def __init__(self, abbrev, electors, initial_votes=(40., 40., 20.)):
    def __init__(self, abbrev, electors):
        super(State, self).__init__()
        self.abbrev = abbrev
        self.name = state_abbrev[abbrev]
        self.electors = electors
        self.votes = RandomVotes()

        self._republican_bias = 1.

    def get_bias(self):
        """ Return the republican bias """
        return self._republican_bias

    def reset_bias(self):
        """ Reset the republican bias """
        self._republican_bias = 1.0

    def modify_bias(self, modifier):
        """ Modify the republican bias """
        self._republican_bias *= modifier

    def convince_voters(self, party, amount):
        """ Convince an 'amount' of voters to vote for some 'party' """

        if party == REPUBLICANS:
            # Apply bonus to amount to convert.
            # Bonus is multiplicative as it is the bias towards republicans.
            real_amount = amount * self._republican_bias

            # Count the number of voters to convert from each voter pool.
            # Conversion is proportional to each pool size.
            non_party_voters = self.votes.undecided + self.votes.democrats
            if non_party_voters > 0:
                undecided_convinced = real_amount * (self.votes.undecided / non_party_voters)
                democrats_convinced = real_amount * (self.votes.democrats / non_party_voters)
            else:  # Handle the case where you REALLY want to win a specific state
                undecided_convinced = 0.0
                democrats_convinced = 0.0

            self.votes.transfer(UNDECIDED, REPUBLICANS, undecided_convinced)
            self.votes.transfer(DEMOCRATS, REPUBLICANS, democrats_convinced)

        else:
            # Apply bonus to amount to convert.
            # Bonus is divisive as it is the bias towards republicans.
            real_amount = amount / self._republican_bias

            # Count the number of voters to convert from each voter pool.
            # Conversion is proportional to each pool size.
            non_party_voters = self.votes.undecided + self.votes.republicans
            undecided_convinced = real_amount * (self.votes.undecided / non_party_voters)
            republicans_convinced = real_amount * (self.votes.republicans / non_party_voters)

            self.votes.transfer(UNDECIDED, DEMOCRATS, undecided_convinced)
            self.votes.transfer(REPUBLICANS, DEMOCRATS, republicans_convinced)

    def stop_the_count(self):
        """ Assign undecided voters to a side """
        # Assign at random within the undecided interval.
        # Number of voters that will chose democrats is divided by republican bias.
        # This means that a +100% bonus will reduce by 2 the number of undecided voters that will chose democrats.
        undecided_to_dem = uniform(0.0, self.votes.undecided) / self._republican_bias
        undecided_to_rep = self.votes.undecided - undecided_to_dem

        # Update the state polls with no more undecided people
        self.votes = Votes(
            self.votes.democrats + undecided_to_dem,
            self.votes.republicans + undecided_to_rep,
            0.0
        )

    def __str__(self):
        """ For debug: print state info and current vote status """
        return f"State of {self.name}: {self.electors} electors. " \
               f"\n\tCurrent votes: {self.votes}."


class Country:
    """ A collection of free states """

    def __init__(self, name, info):
        self.name = name
        self.states = {}

        self._create_states(info)

    def stop_the_count(self):
        """ Order states to stop counting """
        for state in self:
            state.stop_the_count()

    def count_electors(self):
        """ Count the number of electors for each party in all country's states """
        electors = {DEMOCRATS: 0, REPUBLICANS: 0, UNDECIDED: 0}
        for state in self:
            if state.votes.democrats > state.votes.republicans + state.votes.undecided:
                electors[DEMOCRATS] += state.electors
            elif state.votes.republicans > state.votes.democrats + state.votes.undecided:
                electors[REPUBLICANS] += state.electors
            else:
                electors[UNDECIDED] += state.electors
        return electors

    def drop_states(self, to_keep):
        """ Deletes all states from the country, except those in the 'to_keep' list """
        new_states = {k: self.states[k] for k in to_keep}
        self.states = new_states

    def _create_states(self, info):
        """ Create the list of all states in the country """
        for name_abbrev, elector_count in info.items():
            self.states[name_abbrev] = State(name_abbrev, elector_count)

    def __iter__(self):
        """ Lazy way to use class as a list of states (don't care about state names) """
        return iter(self.states.values())

    def __str__(self):
        """ For debug - print results """
        _str = f"** {self.name} election status: **"
        for (_, v) in self.states.items():
            _str += f"\n{v}"
        return _str
