
from gameplay.geopolitics import Country, REPUBLICANS, DEMOCRATS, RALLY_VALUE
from gameplay.resources import geopolitics_info, state_list
from gameplay.actions import Action, FakeNews, PowerStatement
from gameplay.action_list import all_actions
from gameplay.ai_player import select_rally_place

from helpers.rand import choice, randint

MAX_ACTIONS = 4


class Game:
    """
    The game engine:
     - the country geopolitics information
     - the list of possible actions
     - the list of active boosts and vote swing effects
     - the places where each side will have their next campaign rally
    """

    def __init__(self, random=False):
        self.country = Country("USA", geopolitics_info)

        self.fake_news = []
        self.statements = []
        self.rally_state = {REPUBLICANS: None, DEMOCRATS: None}

        self.possible_actions = []
        self.all_actions = all_actions
        self._pick_actions()

        self.counts_stopped = False
        self.nb_turns = 0
        self.max_turns = 10

    def get_vote_count(self):
        return self.country.count_electors()

    def take_turn(self, action=None):
        """
        A game turn:
        - the player selects one (or more) fake news or power statement to play
        - the player selects where to go to wa ... campaign
        - the opponents selects where to go to campaign
        [ news are spread, meetings are held ]
        - the polls are updated
        """
        if self.counts_stopped:
            return

        self._organize_rally(REPUBLICANS)
        self._organize_rally(DEMOCRATS)

        self._pick_actions()
        self._update_polls()

        self.nb_turns += 1
        if self.nb_turns == self.max_turns:
            self.stop_the_count()

    def _pick_actions(self):
        missing = MAX_ACTIONS - len(self.possible_actions)
        for i in range(missing):
            action_index = randint(0, len(self.all_actions)-1)
            action = self.all_actions.pop(action_index)
            self.possible_actions.append(action)

    def play(self, action_index, rally_state):
        action = self.possible_actions.pop(action_index)
        self._add_effect(action.effect)
        self.rally_state[REPUBLICANS] = rally_state
        return action, rally_state

    def add_action(self, action):
        self._add_effect(action.effect)

    def stop_the_count(self):
        """ Stop the game and count the electors """
        if self.counts_stopped:
            return

        self.counts_stopped = True
        self.country.stop_the_count()

    def _organize_rally(self, party):
        if party == REPUBLICANS:
            action = Action(
                "Going to campaign.",
                "We need to keep America great !",
                PowerStatement(self.rally_state[REPUBLICANS].abbrev, RALLY_VALUE)
            )
        else:
            state = select_rally_place(self.country)
            action = Action(
                "Going to campaign.",
                "No bullshit !",
                PowerStatement(state, RALLY_VALUE)
            )

        self._add_effect(action.effect)

    def _update_polls(self):
        """
        Update the voting polls.

        It happens as follow:
        - the state voting bonuses are reset
        - all voting bonuses are re-computed
        - voting effects (vote transfer) are applied
        """
        for state in self.country:
            state.reset_bias()

        # Apply the "boost to vote changes" effects. then empty the list.
        # Then, for boosts still valid after application, add them to the list for next turn.
        for effect in self.fake_news:  # type: FakeNews
            effect.apply()
        self.fake_news = [boost for boost in self.fake_news if boost.alive()]

        # Apply the "vote count" effects, then empty the list.
        for effect in self.statements:  # type: PowerStatement
            effect.apply()
        self.statements = []

    def _add_effect(self, effect):
        """ Add an effect to the list of effects to apply during next turn """
        effect.link_targets(self.country)
        if isinstance(effect, FakeNews):
            self.fake_news.append(effect)
        if isinstance(effect, PowerStatement):
            self.statements.append(effect)

    def __str__(self):
        return str(self.country)
